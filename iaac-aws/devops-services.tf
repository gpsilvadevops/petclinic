/*
  DevOps Services
*/
resource "aws_security_group" "private-sg" {
    name = "vpc_private"
    description = "Allow incoming database connections."

    ingress { # Gitlab
        from_port = 80
        to_port = 80
        protocol = "tcp"
        security_groups = ["${aws_security_group.public-sg.id}"]
    }
    ingress { # Nexus
        from_port = 8081
        to_port = 8081
        protocol = "tcp"
        security_groups = ["${aws_security_group.public-sg.id}"]
    }
    ingress { # Registry
        from_port = 5000
        to_port = 5000
        protocol = "tcp"
        security_groups = ["${aws_security_group.public-sg.id}"]
    }
    ingress { # Sonar
        from_port = 9000
        to_port = 9000
        protocol = "tcp"
        security_groups = ["${aws_security_group.public-sg.id}"]
    }
    ingress { # Sonar
        from_port = 9092
        to_port = 9092
        protocol = "tcp"
        security_groups = ["${aws_security_group.public-sg.id}"]
    }
    ingress { # Tomcat
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        security_groups = ["${aws_security_group.public-sg.id}"]
    }    
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["${var.vpc_cidr}"]
    }
    ingress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = ["${var.vpc_cidr}"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    vpc_id = "${aws_vpc.default.id}"

    tags {
        Name = "Petclinic - Private SG"
    }
}

resource "aws_instance" "devops-services" {
    ami = "${lookup(var.amis, var.aws_region)}"
    availability_zone = "us-east-1a"
    instance_type = "t2.large"
    key_name = "${var.aws_key_name}"
    vpc_security_group_ids = ["${aws_security_group.private-sg.id}"]
    subnet_id = "${aws_subnet.us-east-1a-private.id}"
    private_ip = "192.168.2.100"
    source_dest_check = false
    user_data     = "${data.template_file.script.rendered}"
 
    tags {
        Name = "Petclinic - DevOps Services"
    }
}
